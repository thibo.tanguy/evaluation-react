const API_DEV_URL = 'https://dummyjson.com';

export async function getCartById(cartId) {
    try {
        const response = await fetch(`${API_DEV_URL}/carts/${cartId}`);
        if (response.ok) {
            let data = await response.json();
            return data.products;
        }
    } catch (error) {
        throw error;
    }
}

export async function addCart(userId, products) {
    try {
        const response = await fetch('https://dummyjson.com/carts/add', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                userId,
                products,
            }),
        });
        if (response.ok) {
            return await response.json();
        }
    } catch (error) {
        throw error;
    }
}

export async function getUserCarts(userId) {
    try {
        const response = await fetch(`https://dummyjson.com/carts/user/${userId}`);
        if (response.ok) {
            let data = await response.json();
            return data.carts[0].products;
        }
    } catch (error) {
        throw error;
    }
}

export async function deleteCart(cartId) {
    try {
        const response = await fetch(`https://dummyjson.com/carts/${cartId}`, {
            method: 'DELETE',
        });
        if (response.ok) {
            return await response.json();
        }
    } catch (error) {
        throw error;
    }
}