const API_PROD_URL = '/api';
const API_DEV_URL = 'https://dummyjson.com';

export async function getAllProducts() {
    const response = await fetch(`${API_DEV_URL}/products`);
    let data = await response.json();
    return data.products;
}

export async function getProductById(id) {
    const response = await fetch(`${API_DEV_URL}/products/${id}`);
    let data = await response.json();
    return data;
}

export async function saveProduct(product) {
    await fetch(`${API_DEV_URL}/products/add`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(product),
    });
}

export async function updateProduct(product) {
    await fetch(`${API_DEV_URL}/products/${product.id}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(product),
    });
}

export async function deleteProduct(productId) {
    await fetch(`${API_DEV_URL}/products/${productId}`, {
        method: 'DELETE',
    });
}

export async function searchProduct(term) {
    const response = await fetch(`${API_DEV_URL}/products/search?q=${term}`);
    let data = await response.json();
    return data.products;
}

