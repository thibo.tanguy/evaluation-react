import './App.css'
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import Home from "./pages/Home.jsx";
import ProductDetails from "./pages/ProductDetails.jsx";
import Sell from "./pages/Sell.jsx";
import Cart from "./pages/Cart.jsx";
import Products from "./pages/Products.jsx";
import Navbar from "./components/Navbar.jsx";
import Footer from "./components/Footer.jsx";

function App() {

    return (
        <>
            <div className="App">
                <Router>
                    <header>
                        <Navbar/>
                    </header>

                    <main>
                        <Routes>
                            <Route path="/" element={<Home/>}/>
                            <Route path="products" element={<Products/>}/>
                            <Route path="products/:id" element={<ProductDetails/>}/>
                            <Route path="products/sell" element={<Sell/>}/>
                            <Route path="cart" element={<Cart/>}/>
                        </Routes>
                    </main>

                    <footer>
                        <Footer/>
                    </footer>
                </Router>
            </div>
        </>
    )
}

export default App
