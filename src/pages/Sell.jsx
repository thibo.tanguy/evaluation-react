import React, {useState} from 'react';
import {useForm} from 'react-hook-form';
import {saveProduct} from "../api/ProductsApi.jsx";
import '../assets/styles/sell.css';

const Sell = () => {
    const {register, handleSubmit} = useForm();
    const [successMessage, setSuccessMessage] = useState('');
    const [errorMessage, setErrorMessage] = useState('');

    const onSubmit = async (data) => {
        try {
            await saveProduct(data);
            setSuccessMessage("Nouveau produit enregistré !");
        } catch (error) {
            setErrorMessage("Erreur lors de l'enregistrement du produit.");
        }
    };

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <form onSubmit={handleSubmit(onSubmit)}>
                <label htmlFor="title">Title:</label>
                <input type="text" id="title" {...register('title')} />

                <label htmlFor="description">Description:</label>
                <textarea id="description" {...register('description')} />

                <label htmlFor="price">Price:</label>
                <input type="number" id="price" {...register('price')} />

                {successMessage && <p className="success-message">{successMessage}</p>}
                {errorMessage && <p className="error-message">{errorMessage}</p>}

                <button type="submit">Ajouter</button>
            </form>
        </form>
    );
};

export default Sell;