import React, {useEffect, useState} from "react";
import {
    getAllProducts,
    searchProduct
} from "../api/ProductsApi.jsx";
import ProductsList from "../components/ProductsList.jsx";
import Loading from "../components/Loading.jsx";
import "../assets/styles/products.css"

function Products() {
    const [products, setProducts] = useState([]);
    const [inputValue, setInputValue] = useState("");

    useEffect(() => {
        getAllProducts().then(products => setProducts(products));
    }, []);

    const handleSearch = (e) => {
        setInputValue(e.target.value);
    };

    const handleSubmit = () => {
        if (inputValue === "") {
            getAllProducts().then((products) => setProducts(products));
        } else {
            searchProduct(inputValue).then((products) => setProducts(products));
        }
    };

    if (!products || products.length === 0) {
        return <Loading/>;
    }

    return (
        <div>
            <div className="search-bar">
                <input
                    className="search-input"
                    type="text"
                    value={inputValue}
                    onChange={handleSearch}
                    placeholder="Rechercher un produit..."
                />
                <button className="search-button" onClick={handleSubmit}>
                    Rechercher
                </button>
            </div>

            <ProductsList products={products} />
        </div>
    );
}

export default Products;