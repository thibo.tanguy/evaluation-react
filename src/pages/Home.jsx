import React from 'react';
import '../assets/styles/productItem.css';
import Products from './Products.jsx';
import commerceImage from '../assets/images/e-commerce.png';
import '../assets/styles/home.css';

function Home() {
    return (
        <div className="home-content">
            <div className="home-informations">
                <h1>Bienvenue !</h1>
                <img src={commerceImage} alt="e-commerce" width="400" />
            </div>
            <Products/>
        </div>
    );
}

export default Home;
