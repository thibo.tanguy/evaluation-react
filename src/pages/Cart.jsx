import React, { useEffect, useState } from "react";
import { addCart, getCartById } from "../api/CartsApi.jsx";
import CartProductItem from "../components/CartProductItem.jsx";
import Loading from "../components/Loading.jsx";
import '../assets/styles/cart.css';

function Cart() {
    const [products, setProducts] = useState([]);

    useEffect(() => {
        getCartById(2).then(products => setProducts(products));
    }, []);

    const handleRemoveProduct = (productId) => {
        const updatedProducts = products.filter((product) => product.id !== productId);
        setProducts(updatedProducts);
    };

    const handleValidateCart = () => {
        addCart(2, products).then(() => {
            alert("Panier validé !");
        });
    };

    if (!products || products.length === 0) {
        return <Loading />;
    }

    return (
        <div className="cart-container">
            <h1>Panier</h1>
            {products.map(product =>
                <CartProductItem
                    key={product.id}
                    product={product}
                    onRemove={() => handleRemoveProduct(product.id)}
                />)}

            <button onClick={handleValidateCart} type="submit">
                Valider le panier
            </button>
        </div>
    );
}

export default Cart;
