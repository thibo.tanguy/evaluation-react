import {useEffect, useState} from "react";
import {getProductById} from "../api/ProductsApi.jsx";
import {useParams} from "react-router-dom";
import '../assets/styles/productDetails.css';

function ProductDetails() {
    const [product, setProduct] = useState();
    const params = useParams();

    useEffect(() => {
        getProductById(params.id).then(product => setProduct(product));
    }, []);

    if (!product) {
        return <div>Chargement...</div>;
    }

    return (
        <div className="product-details">
            <img className="product-image" src={product.images[0]} alt={product.title} />
            <div className="product-info">
                <h2>{product.title}</h2>
                <p>{product.description}</p>
                <div className="product-details-info">
                    <p>
                        <strong>Note:</strong> {product.rating}
                    </p>
                    <p>
                        <strong>Marque:</strong> {product.brand}
                    </p>
                    <p>
                        <strong>Catégorie:</strong> {product.category}
                    </p>
                    <p>
                        <strong>Réduction:</strong> {product.discountPercentage}%
                    </p>
                    <p className="product-price">
                        <strong>Prix:</strong> {product.price} €
                    </p>
                </div>
            </div>
        </div>
    );
}

export default ProductDetails;
