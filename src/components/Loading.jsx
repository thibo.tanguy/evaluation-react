import React from 'react';
import "../assets/styles/loading.css"

function Loading() {
    return (
        <div className="loading-container">
            <div className="loading-circle"></div>
        </div>
    );
}

export default Loading;
