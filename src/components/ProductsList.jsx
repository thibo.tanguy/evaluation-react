import ProductItem from "./ProductItem.jsx";
import '../assets/styles/productsList.css';

function ProductsList({products}) {
    return (
        <ul className="products-list">
            {products.map(product => <ProductItem key={product.id} product={product}/>)}
        </ul>
    );
}

export default ProductsList;
