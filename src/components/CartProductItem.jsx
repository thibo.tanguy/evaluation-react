import '../assets/styles/cartProducItem.css';

function CartProductItem({ product, onRemove }) {
    return (
        <div className="cart-product-info">
            <h2>{product.title}</h2>
            <p className="product-price">Prix : {product.price} €</p>
            <p className="product-quantity">Quantité : {product.quantity}</p>
            <p className="product-total">Total : {product.total} €</p>
            <button onClick={onRemove} className="remove-button">
                Supprimer
            </button>
        </div>
    );
}

export default CartProductItem;
