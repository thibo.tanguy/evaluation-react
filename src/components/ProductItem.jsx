import '../assets/styles/productItem.css';
import {Link} from "react-router-dom";

function ProductItem({product}) {
    return (
        <li className="product-item">
            <Link to={`/products/${product.id}`} className="product-link">
                <img className="product-image" src={product.images[0]} alt={product.title}/>
                <div className="product-info">
                    <h2>{product.title}</h2>
                    <p>{product.quantity}</p>
                    <p className="product-price"><strong>Prix : </strong>{product.price} €</p>
                </div>
            </Link>
        </li>
    );
}

export default ProductItem;
