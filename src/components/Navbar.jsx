import { Link } from "react-router-dom";
import '../assets/styles/navbar.css';

function Navbar() {
    return (
        <nav className="navbar">
            <h1>Evaluation React</h1>
            <ul>
                <li><Link to={`/`} className="product-link">Accueil</Link></li>
                <li><Link to={`/products`} className="product-link">Produits</Link></li>
                <li><Link to={`/products/sell`} className="product-link">Ajouter</Link></li>
                <li><Link to={`/cart`} className="product-link">Panier</Link></li>
            </ul>
        </nav>
    );
}

export default Navbar;
